include edo_hi/modify-vendor-before-push.sh

stop:
	docker-compose stop
shell:
	docker-compose exec app sh
start:
	docker-compose up --detach
destroy:
	docker-compose down --volumes
build:
	docker-compose up --detach --build
seed:
 	docker-compose exec app php artisan db:seed
migrate-f:
	docker-compose exec app php artisan migrate:fresh
migrate:
	docker-compose exec app php artisan migrate
serve:
	docker-compose exec app php artisan serve
tinker:
	docker-compose exec app php artisan tinker
optimize-clear:
	docker-compose exec app php artisan optimize:clear
du:
	docker-compose exec app composer du
my:
	docker-compose exec db mysql -u root -p
rebuild-app:
	docker-compose up -d --no-deps --build app
nrw:
	docker-compose exec app npm run watch
finish:
	@echo "adjusting vendor location, staging, commiting,pushing, adjusting vendor location ..."
	@$(call change_vendor_then_git,$(ARG))