#!/bin/sh

change_vendor_then_git() {
    echo "adjusting vendor location .. \n"
    sed -i '"s/require __DIR__.'/../../vendor/autoload.php';/require __DIR__.'/../vendor/autoload.php';/"' public/index.php
    
    git add .
    git commit -m "$1"
    git push origin dev

    echo "adjusting vendor location .. \n"
    sed -i '"s/require __DIR__.'/../vendor/autoload.php';/require __DIR__.'/../../vendor/autoload.php';/"' public/index.php
}
